package com.tikal.atm.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tikal.atm.dao.InventoryDAO;
import com.tikal.atm.dto.CashDepositDTO;
import com.tikal.atm.dto.InventoryDTO;
import com.tikal.atm.entity.Bill;
import com.tikal.atm.entity.Coin;
import com.tikal.atm.entity.Inventory;
import com.tikal.atm.service.InventoryService;
import com.tikal.atm.web.InventoryController;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(value = InventoryController.class)
@AutoConfigureMockMvc
public class InventoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InventoryService inventoryService;

    @Test
    public void testCashWithdrawal() throws Exception {
        ModelMapper mapper = new ModelMapper();
        List<Bill> bills = Arrays.asList(new Bill("200", 4),
                new Bill("100", 2),
                new Bill("50",4), new Bill("20", 4));
        List<Coin> coins = Arrays.asList(new Coin("5", 4),
                new Coin("10",2),
                new Coin("2",2),
                new Coin("1",2),
                new Coin("0.5",2),
                new Coin("0.10",2));

        Inventory inventory = new Inventory(LocalDate.now(),500.50,bills,coins);
        InventoryDTO inventoryDTO = mapper.map(inventory,InventoryDTO.class);
        Mockito.when(inventoryService.cashWithdrawal(500.50)).thenReturn(Optional.of(inventoryDTO).get());
        mockMvc.perform(get("/atm/api/v1/inventory/{amount}",500.50)).andExpect(status().isOk()).andExpect(jsonPath("$.*", Matchers.hasSize(3)))
                .andExpect(jsonPath("$.requestedAmount", Matchers.equalTo(500.50)));
        Mockito.verify(inventoryService, times(1)).cashWithdrawal(500.50);
    }

    @Test
    public void testDepositCash() throws Exception {
        CashDepositDTO cashDepositDTO = new CashDepositDTO();
        cashDepositDTO.setBillAmount200(2);
        cashDepositDTO.setBillAmount100(3);
        cashDepositDTO.setBillAmount50(3);
        cashDepositDTO.setRequestedAmount(850.0);
        ModelMapper mapper = new ModelMapper();
        List<Bill> bills = Arrays.asList(new Bill("200", 4),
                new Bill("100", 2),
                new Bill("50",4), new Bill("20", 4));
        List<Coin> coins = Arrays.asList(new Coin("5", 4),
                new Coin("10",2),
                new Coin("2",2),
                new Coin("1",2),
                new Coin("0.5",2),
                new Coin("0.10",2));
        Inventory inventory = new Inventory(LocalDate.now(),1326.7,bills,coins);
        InventoryDTO inventoryDTO = mapper.map(inventory,InventoryDTO.class);
        Mockito.when(inventoryService.depositCash(any())).thenReturn(inventoryDTO);
        mockMvc.perform(post("/atm/api/v1/inventory").contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(cashDepositDTO)))
                .andExpect(status().isOk()).andExpect(jsonPath("$.*", Matchers.hasSize(3)))
                .andExpect(jsonPath("$.requestedAmount", Matchers.equalTo(1326.7)));
        Mockito.verify(inventoryService, times(1)).depositCash(any());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
