package com.tikal.atm.web;

import com.tikal.atm.dto.CashDepositDTO;
import com.tikal.atm.dto.InventoryDTO;
import com.tikal.atm.entity.Inventory;
import com.tikal.atm.service.InventoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import java.util.List;

@RestController
@RequestMapping("/atm/api/v1")
@RequiredArgsConstructor
@Validated
public class InventoryController {

    private final InventoryService inventoryService;

    @ApiOperation(value = "View a pages of available inventories" ,response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })

    @GetMapping("/inventory")
    public ResponseEntity<Page<Inventory>> findAll(){
        return ResponseEntity.ok(inventoryService.findAll(0,10));
    }

    @ApiOperation(value = "Deposit cash - post a inventory DTO " ,response = InventoryDTO.class)
    @PostMapping("/inventory")
    public ResponseEntity<InventoryDTO> depositCash(@RequestBody @Valid  CashDepositDTO cashDepositDTO){
        return ResponseEntity.ok(inventoryService.depositCash(cashDepositDTO));
    }

    @ApiOperation(value = "Withdrawal cash - retrieve inventory DTO " ,response = InventoryDTO.class)
    @GetMapping("/inventory/{amount}")
    public ResponseEntity<InventoryDTO> cashWithdrawal(@PathVariable @Max(2000) Double amount){
        return ResponseEntity.ok(inventoryService.cashWithdrawal(amount));
    }



}
