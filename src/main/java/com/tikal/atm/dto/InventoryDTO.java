package com.tikal.atm.dto;

import com.tikal.atm.entity.Bill;
import com.tikal.atm.entity.Coin;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InventoryDTO {

    private Double requestedAmount;
    private List<Bill> bills;
    private List<Coin> coins;
}
