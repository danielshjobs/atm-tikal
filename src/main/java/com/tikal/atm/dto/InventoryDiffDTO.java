package com.tikal.atm.dto;

import com.tikal.atm.entity.Inventory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class InventoryDiffDTO {

    private InventoryDTO inventoryDTO;
    private Inventory inventory;
}
