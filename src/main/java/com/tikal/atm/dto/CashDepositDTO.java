package com.tikal.atm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CashDepositDTO {

    @Min(message = "min amount is 300 shekel", value = 300)
    @Max(message = "max amount is 2000 shekel", value = 2000)
    private Double requestedAmount;
    @Positive(message = "billAmount200 is a required field & should be positive number")
    private int billAmount200;
    @Positive(message = "billAmount100 is a required field & should be positive number")
    private int billAmount100;
    @Positive(message = "billAmount50 is a required field & should be positive number")
    private int billAmount50;
    private int billAmount20;
    private int coinAmount10;
    private int coinAmount5;
    private int coinAmount1;
    private int coinAmount2;
    private int coinAmount05;
    private int coinAmount01;
}
