package com.tikal.atm.dao;

import com.tikal.atm.dto.CashDepositDTO;
import com.tikal.atm.dto.InventoryDTO;
import com.tikal.atm.entity.Bill;
import com.tikal.atm.entity.Coin;
import com.tikal.atm.entity.Inventory;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Component
public class InventoryDAO {

    public InventoryDTO mapCashDepositToInventoryDTO(CashDepositDTO cashDepositDTO){
        List<Bill> bills = Arrays.asList(new Bill("200", cashDepositDTO.getBillAmount200()),
                new Bill("100", cashDepositDTO.getBillAmount100()),
                new Bill("50", cashDepositDTO.getBillAmount50()),
                new Bill("20", cashDepositDTO.getBillAmount20()));
        List<Coin> coins = Arrays.asList(new Coin("10", cashDepositDTO.getCoinAmount10()),
                new Coin("5", cashDepositDTO.getCoinAmount5()),
                new Coin("2", cashDepositDTO.getCoinAmount2()),
                new Coin("1", cashDepositDTO.getCoinAmount1()),
                new Coin("0.5", cashDepositDTO.getCoinAmount05()),
                new Coin("0.10", cashDepositDTO.getCoinAmount01()));
        return new InventoryDTO(cashDepositDTO.getRequestedAmount(),bills,coins);
    }

    public InventoryDTO mapInventoryToInventoryDTO(Inventory inventory){
        ModelMapper mapper = new ModelMapper();
        return mapper.map(inventory, InventoryDTO.class);
    }

    public void mapBillAndCoinDTOToInventory(Inventory inventory, InventoryDTO inventoryDTO){
        List<Bill> billEntity = Arrays.asList(new Bill("200",inventory.getBills().get(0).getBillAmount() + inventoryDTO.getBills().get(0).getBillAmount()),
                new Bill("100",inventory.getBills().get(1).getBillAmount() + inventoryDTO.getBills().get(1).getBillAmount()),
                new Bill("50",inventory.getBills().get(2).getBillAmount() + inventoryDTO.getBills().get(2).getBillAmount()),
                new Bill("20",inventory.getBills().get(2).getBillAmount() + inventoryDTO.getBills().get(2).getBillAmount()));

        List<Coin> coinEntity = Arrays.asList(new Coin("10", inventory.getCoins().get(0).getCoinAmount() + inventoryDTO.getCoins().get(0).getCoinAmount()),
                new Coin("5", inventory.getCoins().get(1).getCoinAmount() + inventoryDTO.getCoins().get(1).getCoinAmount()),
                new Coin("2", inventory.getCoins().get(2).getCoinAmount() + inventoryDTO.getCoins().get(2).getCoinAmount()),
                new Coin("1", inventory.getCoins().get(3).getCoinAmount() + inventoryDTO.getCoins().get(3).getCoinAmount()),
                new Coin("0.5", inventory.getCoins().get(4).getCoinAmount() + inventoryDTO.getCoins().get(4).getCoinAmount()),
                new Coin("0.10", inventory.getCoins().get(5).getCoinAmount() + inventoryDTO.getCoins().get(5).getCoinAmount()));
        inventory.setBills(billEntity);
        inventory.setCoins(coinEntity);
    }

    public List<Inventory> mapBillAndCoinToInventory(){
        List<Bill> bills = Arrays.asList(new Bill("200", 4),
                new Bill("100", 2),
                new Bill("50",4), new Bill("20", 4));//1280
        List<Bill> billsTwo = Arrays.asList(new Bill("200", bills.get(0).getBillAmount()+4),
                new Bill("100", bills.get(1).getBillAmount()+2),
                new Bill("50",bills.get(2).getBillAmount()+4),
                new Bill("20", bills.get(3).getBillAmount()+4));//2560

        List<Coin> coins = Arrays.asList(new Coin("5", 4),
                new Coin("10",2),
                new Coin("2",2),
                new Coin("1",2),
                new Coin("0.5",2),
                new Coin("0.10",2));//46.7
        List<Coin> coinsTwo = Arrays.asList(new Coin("5", coins.get(0).getCoinAmount()+4),
                new Coin("10",coins.get(1).getCoinAmount()+2),
                new Coin("2",coins.get(2).getCoinAmount()+2),
                new Coin("1",coins.get(3).getCoinAmount()+2),
                new Coin("0.5",coins.get(4).getCoinAmount()+2),
                new Coin("0.10",coins.get(5).getCoinAmount()+2));
       Inventory inventory = new Inventory(LocalDate.now(),1326.7,bills,coins);
       Inventory inventoryTwo = new Inventory(LocalDate.now(),2654.4,billsTwo,coinsTwo);
       return Arrays.asList(inventory,inventoryTwo);

    }
    public CashDepositDTO mapInventoryAndAmountToCashDepositDTO(Double amount, Inventory inventory){
        Double currentRemainder = amount;
        CashDepositDTO cashDepositDTO = new CashDepositDTO();
        cashDepositDTO.setRequestedAmount(amount);
        if(inventory.getBills().get(0).getBillAmount() > 0){
            cashDepositDTO.setBillAmount200((int) (amount / 200));
            currentRemainder =  amount % 200;
        }if(inventory.getBills().get(1).getBillAmount() > 0){
            cashDepositDTO.setBillAmount100((int) (currentRemainder / 100));
            currentRemainder = currentRemainder % 100;
        }if(inventory.getBills().get(2).getBillAmount() > 0){
            cashDepositDTO.setBillAmount50((int) (currentRemainder / 50));
            currentRemainder = currentRemainder % 50;
        }if(inventory.getBills().get(3).getBillAmount() > 0){
            cashDepositDTO.setBillAmount20((int) (currentRemainder / 20));
            currentRemainder = currentRemainder % 20;
        }if(inventory.getCoins().get(0).getCoinAmount() > 0){
            cashDepositDTO.setCoinAmount10((int) (currentRemainder / 10));
            currentRemainder = currentRemainder % 10;
        }if(inventory.getCoins().get(1).getCoinAmount() > 0){
            cashDepositDTO.setCoinAmount5((int) (currentRemainder / 5));
            currentRemainder = currentRemainder % 5;
        }if(inventory.getCoins().get(2).getCoinAmount() > 0){
            cashDepositDTO.setCoinAmount2((int) (currentRemainder / 2));
            currentRemainder = currentRemainder % 2;
        }if(inventory.getCoins().get(3).getCoinAmount() > 0){
            cashDepositDTO.setCoinAmount1((int) (currentRemainder / 1));
            currentRemainder = currentRemainder % 1;
        }if(inventory.getCoins().get(4).getCoinAmount() > 0){
            cashDepositDTO.setCoinAmount05((int) (currentRemainder / 0.5));
            currentRemainder = currentRemainder % 0.5;
        }if(inventory.getCoins().get(5).getCoinAmount() > 0){
            cashDepositDTO.setCoinAmount01((int) (currentRemainder / 0.1));
            currentRemainder = currentRemainder % 0.1;
        }
        return cashDepositDTO;
    }

}
