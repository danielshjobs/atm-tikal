package com.tikal.atm.dao;

import com.tikal.atm.dto.CashDepositDTO;
import com.tikal.atm.dto.InventoryDTO;
import com.tikal.atm.dto.InventoryDiffDTO;
import com.tikal.atm.entity.Inventory;
import com.tikal.atm.errorhandler.TooMuchCoinsException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DiffDAO {

    private final InventoryDAO inventoryDAO;
    //calculation financial diff of our cash amount
    public InventoryDiffDTO calculateDiff(InventoryDTO inventoryDTO, Inventory inventory){
        int diffAmount = 0;
        int i = 0;
        if(inventory.getBills().get(0).getBillAmount() > inventoryDTO.getBills().get(0).getBillAmount() ){
            inventory.getBills().get(0).setBillAmount(inventory.getBills().get(0).getBillAmount() - inventoryDTO.getBills().get(0).getBillAmount());
            inventory.setRequestedAmount(inventory.getRequestedAmount() - inventoryDTO.getBills().get(0).getBillAmount() * 200);
        }else if(inventory.getBills().get(0).getBillAmount() < inventoryDTO.getBills().get(0).getBillAmount() || inventory.getBills().get(0).getBillAmount() > 0){
            diffAmount = inventoryDTO.getBills().get(0).getBillAmount() - inventory.getBills().get(0).getBillAmount();
            inventoryDTO.getBills().get(0).setBillAmount(inventory.getBills().get(0).getBillAmount());
            inventory.getBills().get(0).setBillAmount(0);
            inventory.setRequestedAmount(inventory.getRequestedAmount() - inventory.getBills().get(0).getBillAmount() * 200);

        }if(inventory.getBills().get(1).getBillAmount() > inventoryDTO.getBills().get(1).getBillAmount()){
            inventory.getBills().get(1).setBillAmount(inventory.getBills().get(1).getBillAmount() - inventoryDTO.getBills().get(1).getBillAmount());
            inventory.setRequestedAmount(inventory.getRequestedAmount() - inventoryDTO.getBills().get(1).getBillAmount() * 100);
        } else if(inventory.getBills().get(1).getBillAmount() < inventoryDTO.getBills().get(1).getBillAmount() || inventory.getBills().get(1).getBillAmount() > 0){
            diffAmount = inventoryDTO.getBills().get(1).getBillAmount() - inventory.getBills().get(1).getBillAmount();
            inventoryDTO.getBills().get(1).setBillAmount(inventory.getBills().get(1).getBillAmount());
            inventory.getBills().get(1).setBillAmount(0);
            inventory.setRequestedAmount(inventory.getRequestedAmount() - inventory.getBills().get(1).getBillAmount() * 100);

        }if(inventory.getBills().get(2).getBillAmount() > inventoryDTO.getBills().get(2).getBillAmount()){
            inventory.getBills().get(2).setBillAmount(inventory.getBills().get(2).getBillAmount() - inventoryDTO.getBills().get(2).getBillAmount());
            inventory.setRequestedAmount(inventory.getRequestedAmount() - inventoryDTO.getBills().get(2).getBillAmount() * 50);
        }else if(inventory.getBills().get(2).getBillAmount() < inventoryDTO.getBills().get(2).getBillAmount() || inventory.getBills().get(2).getBillAmount() > 0){
            diffAmount = inventoryDTO.getBills().get(2).getBillAmount() - inventory.getBills().get(2).getBillAmount();
            inventoryDTO.getBills().get(2).setBillAmount(inventory.getBills().get(2).getBillAmount());
            inventory.getBills().get(2).setBillAmount(0);
            inventory.setRequestedAmount(inventory.getRequestedAmount() - inventory.getBills().get(2).getBillAmount() * 50);
        }
        if(inventory.getBills().get(3).getBillAmount() > inventoryDTO.getBills().get(3).getBillAmount()){
            inventory.getBills().get(3).setBillAmount(inventory.getBills().get(3).getBillAmount() - inventoryDTO.getBills().get(3).getBillAmount());
            inventory.setRequestedAmount(inventory.getRequestedAmount() - inventoryDTO.getBills().get(3).getBillAmount() * 20);
        }else if(inventory.getBills().get(3).getBillAmount() < inventoryDTO.getBills().get(3).getBillAmount() || inventory.getBills().get(3).getBillAmount() > 0) {
            diffAmount = inventoryDTO.getBills().get(3).getBillAmount() - inventory.getBills().get(3).getBillAmount();
            inventoryDTO.getBills().get(3).setBillAmount(inventory.getBills().get(3).getBillAmount());
            inventory.getBills().get(3).setBillAmount(0);
            inventory.setRequestedAmount(inventory.getRequestedAmount() - inventory.getBills().get(3).getBillAmount() * 20);

            //bills over, too many coins to retrieve
        }else {
            throw new TooMuchCoinsException("Too much coins to single withdrawal");
        }
        //Recalculate if there is any gap
        if(diffAmount > 0 && i == 0 ){
            i++;
            CashDepositDTO cashDepositDTO =  inventoryDAO.mapInventoryAndAmountToCashDepositDTO(Double.valueOf(diffAmount*100),inventory);
            inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
            calculateDiff(inventoryDTO, inventory);
        }else if(diffAmount > 0 && i == 1 ){
            i++;
            CashDepositDTO cashDepositDTO = inventoryDAO.mapInventoryAndAmountToCashDepositDTO(Double.valueOf(diffAmount*50),inventory);
            inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
            calculateDiff(inventoryDTO, inventory);
        }else if(diffAmount > 0 && i == 2){
            i++;
            CashDepositDTO cashDepositDTO = inventoryDAO.mapInventoryAndAmountToCashDepositDTO(Double.valueOf(diffAmount*20),inventory);
            inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
            calculateDiff(inventoryDTO, inventory);
        }else if(diffAmount > 0 && i == 3){
            i++;
            CashDepositDTO cashDepositDTO = inventoryDAO.mapInventoryAndAmountToCashDepositDTO(Double.valueOf(diffAmount*10),inventory);
            inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
            calculateDiff(inventoryDTO, inventory);
        }else if(diffAmount > 0 && i == 4){
            i++;
            CashDepositDTO cashDepositDTO = inventoryDAO.mapInventoryAndAmountToCashDepositDTO(Double.valueOf(diffAmount*5),inventory);
            inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
            calculateDiff(inventoryDTO, inventory);
        }else if(diffAmount > 0 && i == 5){
            i++;
            CashDepositDTO cashDepositDTO = inventoryDAO.mapInventoryAndAmountToCashDepositDTO(Double.valueOf(diffAmount*2),inventory);
            inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
            calculateDiff(inventoryDTO, inventory);
        }else if(diffAmount > 0 && i == 6){
            i++;
            CashDepositDTO cashDepositDTO = inventoryDAO.mapInventoryAndAmountToCashDepositDTO(Double.valueOf(diffAmount*1),inventory);
            inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
            calculateDiff(inventoryDTO, inventory);
        }else if(diffAmount > 0 && i == 7){
            i++;
            CashDepositDTO cashDepositDTO = inventoryDAO.mapInventoryAndAmountToCashDepositDTO(Double.valueOf(diffAmount*0.5),inventory);
            inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
            calculateDiff(inventoryDTO, inventory);
        }else if(diffAmount > 0 && i == 8){
            i++;
            CashDepositDTO cashDepositDTO = inventoryDAO.mapInventoryAndAmountToCashDepositDTO(Double.valueOf(diffAmount*0.10),inventory);
            inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
            calculateDiff(inventoryDTO, inventory);
        }

        return new InventoryDiffDTO(inventoryDTO,inventory);
    }
}
