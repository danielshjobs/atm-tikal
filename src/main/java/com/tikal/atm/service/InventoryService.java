package com.tikal.atm.service;

import com.tikal.atm.dao.DiffDAO;
import com.tikal.atm.dao.InventoryDAO;
import com.tikal.atm.dto.CashDepositDTO;
import com.tikal.atm.dto.CashWithdrawalDTO;
import com.tikal.atm.dto.InventoryDTO;
import com.tikal.atm.dto.InventoryDiffDTO;
import com.tikal.atm.entity.Inventory;
import com.tikal.atm.errorhandler.ConflictException;
import com.tikal.atm.repository.InventoryRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class InventoryService {

    private final InventoryRepository inventoryRepository;
    private final SequenceGeneratorService sequenceGenerator;
    private final InventoryDAO inventoryDAO;
    private final DiffDAO diffDAO;

    //Deposit cash to ATM
    public InventoryDTO depositCash(CashDepositDTO cashDepositDTO){
        InventoryDTO inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
        Inventory inventory = findAll(0,1).getContent().get(0);
        inventoryDAO.mapBillAndCoinDTOToInventory(inventory,inventoryDTO);
        inventory.setRequestedAmount(inventory.getRequestedAmount()+inventoryDTO.getRequestedAmount());
        inventory.setInsertTime(LocalDate.now());
        inventory.setId(sequenceGenerator.generateSequence(Inventory.SEQUENCE_NAME));
        inventoryRepository.insert(inventory);
        return inventoryDTO;

    }
    //Withdrawal cash from the ATM
    public InventoryDTO cashWithdrawal(Double amount){
        Inventory inventory = findAll(0,1).getContent().get(0);
        CashDepositDTO cashDepositDTO = calculateAmount(amount, inventory);
        InventoryDTO inventoryDTO = inventoryDAO.mapCashDepositToInventoryDTO(cashDepositDTO);
        if(inventory.getRequestedAmount() < inventoryDTO.getRequestedAmount()){
            throw new ConflictException("There is no enough money in the ATM"+ inventory.getRequestedAmount());
        }else{
            inventoryDTO = calculateDiff(inventoryDTO,inventory);
        }
        return inventoryDTO;
    }

    //Calculate amount via Bills & Coins preferred bill
    public CashDepositDTO calculateAmount(Double amount, Inventory inventory){
       return inventoryDAO.mapInventoryAndAmountToCashDepositDTO(amount,inventory);
    }

    public InventoryDTO calculateDiff(InventoryDTO inventoryDTO, Inventory inventory){
        System.out.println("passed"+inventoryDTO);
        InventoryDiffDTO inventoryDiffDTO = diffDAO.calculateDiff(inventoryDTO,inventory);
        inventoryDiffDTO.getInventory().setInsertTime(LocalDate.now());
        inventoryDiffDTO.getInventory().setId(sequenceGenerator.generateSequence(Inventory.SEQUENCE_NAME));
        inventoryRepository.insert(inventoryDiffDTO.getInventory());
        return inventoryDiffDTO.getInventoryDTO();
    }

   //Deposit cash on pre seeded
   @PostConstruct
    public void insertPreSeeded(){
        Page inventoryPage = findAll(0,1);
        // Making deposit - Only if there is no cash in the ATM
        if(inventoryPage.getTotalElements() == 0){
            Inventory inventory = inventoryDAO.mapBillAndCoinToInventory().get(0);
            inventory.setId(sequenceGenerator.generateSequence(Inventory.SEQUENCE_NAME));
            Inventory inventoryTwo =  inventoryDAO.mapBillAndCoinToInventory().get(1);
            inventoryTwo.setId(sequenceGenerator.generateSequence(Inventory.SEQUENCE_NAME));
            List<Inventory> inventories = List.of(inventory,inventoryTwo);
            inventoryRepository.insert(inventories);
        }
        log.info("Cash has been loaded");

    }
    //Retrieve the most updatable record via pagination
    public Page<Inventory> findAll(int page, int size){
        Pageable paging = PageRequest.of(page,size, Sort.by("id").descending());
        return inventoryRepository.findAll(paging);
    }
}
