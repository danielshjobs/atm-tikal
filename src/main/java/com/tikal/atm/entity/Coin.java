package com.tikal.atm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Coin {

    private String coinValue;
    private int coinAmount;
}
