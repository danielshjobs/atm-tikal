package com.tikal.atm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Bill {

    private String billValue;
    private int billAmount;
}
