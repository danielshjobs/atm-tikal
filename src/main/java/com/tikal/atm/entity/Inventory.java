package com.tikal.atm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;

@Document(collection = "inventory")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Inventory {

    @Transient
    public static final String SEQUENCE_NAME = "inventories_sequence";

    @Id
    private Long id;//change to long
    private LocalDate insertTime;
    private Double requestedAmount;
    private List<Bill> bills;
    private List<Coin> coins;

    public Inventory(LocalDate insertTime, Double requestedAmount, List<Bill> bills, List<Coin> coins) {
        this.insertTime = insertTime;
        this.requestedAmount = requestedAmount;
        this.bills = bills;
        this.coins = coins;
    }
}
