package com.tikal.atm.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Too much coins to single withdrawal")
public class TooMuchCoinsException extends RuntimeException {

    public TooMuchCoinsException(){
        super();
    }

    public TooMuchCoinsException(final String message){
        super(message);

    }
}
