package com.tikal.atm.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "There is no enough money in the ATM")
public class ConflictException extends RuntimeException {

    public ConflictException(){
        super();
    }
    public ConflictException(final String message){
        super(message);
    }

}
