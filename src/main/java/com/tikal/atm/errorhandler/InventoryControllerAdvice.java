package com.tikal.atm.errorhandler;

import com.tikal.atm.dto.CashWithdrawalDTO;
import com.tikal.atm.dto.ErrorHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.time.LocalDateTime;

@ControllerAdvice(annotations = RestController.class)
public class InventoryControllerAdvice {

    @ExceptionHandler({ConstraintViolationException.class, ValidationException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorHandler> constraintViolationException(final RuntimeException exception, final WebRequest webrequest,final HttpServletRequest request) {
        ErrorHandler error = new ErrorHandler();
        error.setTimestamp(LocalDateTime.now());
        error.setMessage(exception.getMessage());
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setUri(request.getRequestURI());
        return ResponseEntity.badRequest().body(error);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorHandler> constraintViolationException(final MethodArgumentNotValidException exception, final WebRequest webrequest,final HttpServletRequest request) {
        ErrorHandler error = new ErrorHandler();
        error.setTimestamp(LocalDateTime.now());
        error.setMessage(exception.getMessage());
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setUri(request.getRequestURI());
        return ResponseEntity.badRequest().body(error);
    }

    @ExceptionHandler(ConflictException.class)
    public ResponseEntity<ErrorHandler> conflictException(final ConflictException exception, final WebRequest webRequest,final HttpServletRequest request) {
        ErrorHandler error = new ErrorHandler();
        error.setTimestamp(LocalDateTime.now());
        error.setMessage(exception.getMessage());
        error.setStatus(HttpStatus.CONFLICT.value());
        error.setUri(request.getRequestURI());
        return ResponseEntity.badRequest().body(error);
    }

    @ExceptionHandler(TooMuchCoinsException.class)
    public ResponseEntity<ErrorHandler> tooMuchCoinsException(final TooMuchCoinsException exception, final WebRequest webRequest,final HttpServletRequest request) {
        ErrorHandler error = new ErrorHandler();
        error.setTimestamp(LocalDateTime.now());
        error.setMessage(exception.getMessage());
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setUri(request.getRequestURI());
        return ResponseEntity.badRequest().body(error);
    }
}
