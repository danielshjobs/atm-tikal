package com.tikal.atm.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackages = "com.tikal.atm.repository")
@Configuration
public class MongoConfig {
}
