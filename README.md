REST API example application:

    -   This is API application to expose REST api for deposit & withdrawal cash.
    -   ATM api base on MVC class diagram via the most popular patterns such as :(entity, DTO,DAO,Repository,Service.)
    -   ATM api including variety of artifacts: (Spring data, Acutator, Swagger interface, Pagination.)
    -   ATM developed via product deleviry approch by JIB & helm chart as part from CICD.
    -   ATM project including integration to mongo Atlas DB and all the extra u ask for.

 To Install:

    - Java 11
    - Maven 3.1+   
    - Any IDE recommended intellij ultimate (by intellij the JDK & Maven shuld be build-in) 

 Run the app:

    - Click Run in the IDE.   

 Run test:

    -   mvn test or /.mvnw test
 
 Run clean:      
   
    -   mvn clean

 Run Package:
   
    -   mvn Package   

 REST API - api including there endpoints :
   
    -  post  /atm/api/vi/inventory - to deposit cash to ATM.
    -  Get   /atm/api/vi/inventory/{amount} - to withdrawal cash from ATM.
    -  Get   /atm/api/vi/inventory/ - to display cash in the ATM via pagination.
    - for your convinience, please assist on swagger-ui to invoke the api, below the swaager path:
            -   http://localhost:8080/swagger-ui/#/inventory-controller

 Helm3 Install:
   
    - brew install helm
    - helm upgrade -i "Release" helm/ATM       

JIB:
   
    -   Java Image builder - allow u to create docklize of spring boot java app with no docker file.
    -   if u intersting on deploy please turn the plugin to you HUB, via - mvn install jib:build -Djib.to.auth.username=$DOCKER_USER -Djib.to.auth.password=$DOCKER_PASSWORD -Djib.to.image.=$DOCKER_HOST 


 




